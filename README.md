# Hi Ubuntu-Touch on a Fairphone 4 user 

When you are a Fairphone 4 tester please file your issues here in the [FP4 issue list](https://gitlab.com/ubports/porting/community-ports/android11/fairphone-4/fairphone-fp4/-/issues) or adjust/ fintune /  comment to the existing issues in the list.

**Please check for Features & Usability the Fairphone 4 [devices page](https://devices.ubuntu-touch.io/device/fp4/#portStatus)**
